package cz.ita.javaee.web.controller;

import com.google.common.base.Throwables;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

public class GenericController {
    @ExceptionHandler(Exception.class)
    public @ResponseBody
    ResponseEntity<Error> handleException(Exception ex) {
        ResponseEntity<Error> response = new ResponseEntity<>(new Error(ex.getMessage(), Throwables.getStackTraceAsString(ex)), HttpStatus.BAD_REQUEST);
        return response;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public @ResponseBody
    ResponseEntity<Error> handleIOException(MethodArgumentNotValidException ex) {
        ResponseEntity<Error> response = new ResponseEntity<>(new Error("Validation failed.", Throwables.getStackTraceAsString(ex)), HttpStatus.BAD_REQUEST);
        return response;
    }

    public static class Error {

        public Error(String message, String stacktrace) {
            this.message = message;
            this.stackTrace = stacktrace;
        }

        public String message;
        public String stackTrace;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStackTrace() {
            return stackTrace;
        }

        public void setStackTrace(String stackTrace) {
            this.stackTrace = stackTrace;
        }
    }
}
