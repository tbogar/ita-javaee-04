package cz.ita.javaee.service.api;

import java.util.List;

import cz.ita.javaee.dto.CompanyDto;

public interface CompanyService {

    List<CompanyDto> findAll();

    CompanyDto create(final CompanyDto company);

    CompanyDto update(CompanyDto company);

    void activate(Long id);
}
