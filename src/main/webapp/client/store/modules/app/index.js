import actions from './appActions';
import mutations from './appMutations';

const initialState = {
  submitProtection: false,
  configuration: null
};

export default {
  namespaced: true,
  state: initialState,
  mutations: mutations,
  actions: actions
}
