import * as types from "../../mutationTypes";
import companyService from "../../../services/companyService";
import _ from 'lodash';

const actions = {
  async getAll ({ state, commit}, force) {
    if (!state.items || state.items.length === 0 || force) {
      const companies = await companyService.findAll();
      await commit(types.COMPANY_LIST, {
        items: companies
      });
    }
  },
  async create ({ dispatch, commit}, company) {
    try {
      let response = await companyService.create(company);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async update ({ dispatch, commit}, company) {
    try {
      let response = await companyService.update(company);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async activate ({ state, commit}, id) {
    try {
      await commit(types.COMPANY_ACTIVATE, id);
      let company = _.find(state.items, { id: id});
      let response = await companyService.update(company);
      return !!response.ok;
    } catch (ex) {
      console.error(ex);
    }
  },
  async deactivate ({ state, commit}, id) {
    try {
      await commit(types.COMPANY_DEACTIVATE, id);
      let company = _.find(state.items, { id: id});
      let response = await companyService.update(company);
      return !!response.ok;
    } catch (ex) {
      console.error(ex);
    }
  },
  async delete ({dispatch, commit}, id) {
    let response = await companyService.delete(id);
    if (response.ok) {
      dispatch('getAll', true);
      return true;
    } else {
      return false;
    }
  }
};

export default actions
