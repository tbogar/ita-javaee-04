import companyResource from '../resources/companyResource';
import _ from 'lodash';
import moment from 'moment';

class Company {
  constructor(data) {
    _.merge(this, data);
    this.created = moment(data.created);
  }
}
export default {
  newCompany() {
    return new Company({address: {}});
  },
  async findAll () {
    const response = await companyResource.query();
    if (response.ok) {
      return response.data.map((companyData) => new Company(companyData));
    } else {
      return null;
    }
  },
  async create (company) {
    return companyResource.save({}, _.pickBy(company));
  },
  async update (company) {
    return companyResource.update({}, _.pickBy(company));
  },
  async delete(id) {
    return companyResource.delete({id: id});
  }
}
