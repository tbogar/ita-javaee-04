'use strict';
import confirmMixin from './confirmMixin'
import submitProtectionMixin from './submitProtectionMixin'

export default (store) => {
  confirmMixin();
  submitProtectionMixin(store);
};
