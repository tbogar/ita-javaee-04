import Vue from 'vue'
import Router from 'vue-router'
import overviewView from 'views/overview'
import companiesView from 'views/companies'

Vue.use(Router);

const router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      redirect: '/overview'
    },
    {
      path: '/overview',
      component: overviewView
    },
    {
      path: '/companies',
      component: companiesView,
      meta: {
        requiresLoggedIn: true
      }
    },
    {
      path: '/*',
      redirect: '/'
    }
  ]
});

export default router;

