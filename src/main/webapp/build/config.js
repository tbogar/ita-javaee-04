'use strict';
const pkg = require('../package');

module.exports = {
  port: 4000,
  restEndpoint: {
    host: 'http://localhost',
    port: 8080
  },
  title: 'brainis',
  // when you use electron please set to relative path like ./
  // otherwise only set to absolute path when you're using history mode
  publicPath: '/static/',
  // add these dependencies to a standalone vendor bundle
  vendor: [
    'vue',
    'vuex',
    'vue-router',
    'vue-resource',
    'vue-strap',
    'vue-select',
    'vee-validate',
    'vuex-router-sync',
    'promise-polyfill',
    'numeral',
    'numeral/locales/sk',
    'moment'
  ],
  // disable babelrc by default
  //be carefull of upgrade babel-loader to version 7.1.1 - because LoaderOptionsPlugin doesn't work correctly with it.
  babel: {
    babelrc: false,
    presets: [
      ['es2015', {modules: false}],
      'stage-2'
    ],
    // support jsx in render function
    plugins: [
      'transform-vue-jsx',
      'transform-runtime'
    ]
  },
  postcss: [
    // add prefix via postcss since it's faster
    require('autoprefixer')({
      // Vue does not support ie 8 and below
      browsers: ['last 2 versions', 'ie > 8']
    }),
    require('postcss-nested')
  ],
  cssModules: false,
};
